unit model.answer;

interface

uses
  System.SysUtils, System.Classes, Data.DB;

type
  TModelAnswer = class
  private
    FACTION_TYPE: String;
    FNEXTQUESTION_ID: Integer;
    FQUESTION_ID: Integer;
    FID: Integer;
    FTEXT: String;
    procedure SetACTION_TYPE(const Value: String);
    procedure SetID(const Value: Integer);
    procedure SetNEXTQUESTION_ID(const Value: Integer);
    procedure SetQUESTION_ID(const Value: Integer);
    procedure SetTEXT(const Value: String);

    function DoFind: Boolean;
    procedure LoadProperties(ADataSet: TDataSet);
  public
    property ID: Integer read FID write SetID;
    property TEXT: String read FTEXT write SetTEXT;
    property ACTION_TYPE: String read FACTION_TYPE write SetACTION_TYPE;
    property QUESTION_ID: Integer read FQUESTION_ID write SetQUESTION_ID;
    property NEXTQUESTION_ID: Integer read FNEXTQUESTION_ID write SetNEXTQUESTION_ID;

    class function Find(AQuestionID: Integer; AAnswerText: String): TModelAnswer;
  end;

implementation

{ TAnswer }

uses module.connection;

function TModelAnswer.DoFind: Boolean;
const
  FIND_BY_TEXT = 'SELECT * FROM ANSWER WHERE QUESTION_ID = %d AND TEXT = ''%s''';

var
  LConnection: TConnection;
  LDataSet: TDataSet;
begin
  Result := False;

  LConnection := TConnection.GetConnection;
  try
    LConnection.FDConn.ExecSQL(Format(FIND_BY_TEXT, [Self.QUESTION_ID, Self.TEXT]), LDataSet);
    try
      if not LDataSet.IsEmpty then
      begin
        LoadProperties(LDataSet);
        Result := True;
      end;
    finally
      LDataSet.Free;
    end;
  finally
    LConnection.Free;
  end;
end;

class function TModelAnswer.Find(AQuestionID: Integer; AAnswerText: String): TModelAnswer;
var
  LAnswer: TModelAnswer;
begin
  LAnswer := TModelAnswer.Create;

  LAnswer.QUESTION_ID := AQuestionID;
  LAnswer.TEXT := AAnswerText;
  LAnswer.DoFind;

  Result := LAnswer;
end;

procedure TModelAnswer.LoadProperties(ADataSet: TDataSet);
begin
  ID := ADataSet.FieldByName('ID').AsInteger;
  TEXT := ADataSet.FieldByName('TEXT').AsString;
  ACTION_TYPE := ADataSet.FieldByName('ACTION_TYPE').AsString;
  QUESTION_ID := ADataSet.FieldByName('QUESTION_ID').AsInteger;
  NEXTQUESTION_ID := ADataSet.FieldByName('NEXTQUESTION_ID').AsInteger;
end;

procedure TModelAnswer.SetACTION_TYPE(const Value: String);
begin
  FACTION_TYPE := Value;
end;

procedure TModelAnswer.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TModelAnswer.SetNEXTQUESTION_ID(const Value: Integer);
begin
  FNEXTQUESTION_ID := Value;
end;

procedure TModelAnswer.SetQUESTION_ID(const Value: Integer);
begin
  FQUESTION_ID := Value;
end;

procedure TModelAnswer.SetTEXT(const Value: String);
begin
  FTEXT := Value;
end;

end.
