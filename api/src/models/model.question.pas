unit model.question;

interface

uses
  System.SysUtils, Data.DB;

type
  TModelQuestion = class
  private
    FNEXT_ID: Integer;
    FQUESTION: String;
    FMEDIAURL: String;
    FID: Integer;
    FCLEARDATA: String;
    FSTOREDATA: String;
    procedure SetCLEARDATA(const Value: String);
    procedure SetID(const Value: Integer);
    procedure SetMEDIAURL(const Value: String);
    procedure SetNEXT_ID(const Value: Integer);
    procedure SetQUESTION(const Value: String);
    procedure SetSTOREDATA(const Value: String);

    function DoFind: Boolean;
    procedure LoadProperties(ADataSet: TDataSet);
  public
    property ID: Integer read FID write SetID;
    property QUESTION: String read FQUESTION write SetQUESTION;
    property MEDIAURL: String read FMEDIAURL write SetMEDIAURL;
    property STOREDATA: String read FSTOREDATA write SetSTOREDATA;
    property CLEARDATA: String read FCLEARDATA write SetCLEARDATA;
    property NEXT_ID: Integer read FNEXT_ID write SetNEXT_ID;

    class function Find(AID: Integer): TModelQuestion;
  end;

implementation

{ TQuestion }

uses module.connection;

function TModelQuestion.DoFind: Boolean;
const
  FIND_BY_ID = 'SELECT * FROM QUESTION WHERE ID = %d';

var
  LConnection: TConnection;
  LDataSet: TDataSet;
begin
  Result := False;

  LConnection := TConnection.GetConnection;
  try
    LConnection.FDConn.ExecSQL(Format(FIND_BY_ID, [Self.ID]), LDataSet);
    try
      if not LDataSet.IsEmpty then
      begin
        LoadProperties(LDataSet);
        Result := True;
      end;
    finally
      LDataSet.Free;
    end;
  finally
    LConnection.Free;
  end;
end;

class function TModelQuestion.Find(AID: Integer): TModelQuestion;
var
  LQuestion: TModelQuestion;
begin
  LQuestion := TModelQuestion.Create;

  LQuestion.ID := AID;
  LQuestion.DoFind;

  Result := LQuestion;
end;

procedure TModelQuestion.LoadProperties(ADataSet: TDataSet);
begin
  ID := ADataSet.FieldByName('ID').AsInteger;
  QUESTION := ADataSet.FieldByName('QUESTION').AsString;
  MEDIAURL := ADataSet.FieldByName('MEDIAURL').AsString;
  STOREDATA := ADataSet.FieldByName('STOREDATA').AsString;
  CLEARDATA := ADataSet.FieldByName('CLEARDATA').AsString;
  NEXT_ID := ADataSet.FieldByName('NEXT_ID').AsInteger;
end;

procedure TModelQuestion.SetCLEARDATA(const Value: String);
begin
  FCLEARDATA := Value;
end;

procedure TModelQuestion.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TModelQuestion.SetMEDIAURL(const Value: String);
begin
  FMEDIAURL := Value;
end;

procedure TModelQuestion.SetNEXT_ID(const Value: Integer);
begin
  FNEXT_ID := Value;
end;

procedure TModelQuestion.SetQUESTION(const Value: String);
begin
  FQUESTION := Value;
end;

procedure TModelQuestion.SetSTOREDATA(const Value: String);
begin
  FSTOREDATA := Value;
end;

end.
