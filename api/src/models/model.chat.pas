unit model.chat;

interface

uses
  System.SysUtils, System.Classes, System.StrUtils, Data.DB;

type
  TModelChat = class
  private
    FACTION_TYPE: String;
    FLAST: TDateTime;
    FQUESTION_ID: Integer;
    FFINISH: TDateTime;
    FID: Integer;
    FSTART: TDateTime;
    FCONTACT_ID: Integer;
    FSTOREDDATA: String;
    procedure SetACTION_TYPE(const Value: String);
    procedure SetCONTACT_ID(const Value: Integer);
    procedure SetFINISH(const Value: TDateTime);
    procedure SetID(const Value: Integer);
    procedure SetLAST(const Value: TDateTime);
    procedure SetQUESTION_ID(const Value: Integer);
    procedure SetSTART(const Value: TDateTime);
    procedure SetSTOREDDATA(const Value: String);

    function DoFind: Boolean;
    function DoCreate: Boolean;
    function DoSave: Boolean;
    procedure LoadProperties(ADataSet: TDataSet);
  public
    property ID: Integer read FID write SetID;
    property START: TDateTime read FSTART write SetSTART;
    property LAST: TDateTime read FLAST write SetLAST;
    property FINISH: TDateTime read FFINISH write SetFINISH;
    property ACTION_TYPE: String read FACTION_TYPE write SetACTION_TYPE;
    property STOREDDATA: String read FSTOREDDATA write SetSTOREDDATA;
    property QUESTION_ID: Integer read FQUESTION_ID write SetQUESTION_ID;
    property CONTACT_ID: Integer read FCONTACT_ID write SetCONTACT_ID;

    class function FindOrCreate(AContactID: Integer; out ACreated: Boolean): TModelChat;

    //procedure ClearStoredData;
    function GetData(AData: String): String;
    procedure SetQuestion(AQuestionID: Integer; ADataToStore: String = ''; AClearStoredData: Boolean = False);

    function Save: Boolean;
  end;

implementation

{ TModelChat }

uses module.connection;

function TModelChat.DoCreate: Boolean;
const
  INSERT = 'INSERT INTO CHAT (START, LAST, CONTACT_ID) VALUES (:START, :LAST, :CONTACT_ID)';

var
  LConnection: TConnection;
begin
  LConnection := TConnection.GetConnection;
  try
    Result := LConnection.FDConn.ExecSQL(INSERT, [Now, Now, Self.CONTACT_ID], [ftDateTime, ftDateTime, ftInteger]) > 0;
  finally
    LConnection.Free;
  end;
end;

function TModelChat.DoFind: Boolean;
const
  FIND_BY_CONTACT_ID = 'SELECT * FROM CHAT WHERE CONTACT_ID = %d AND FINISH IS NULL';

var
  LConnection: TConnection;
  LDataSet: TDataSet;
begin
  Result := False;

  LConnection := TConnection.GetConnection;
  try
    LConnection.FDConn.ExecSQL(Format(FIND_BY_CONTACT_ID, [Self.CONTACT_ID]), LDataSet);
    try
      if not LDataSet.IsEmpty then
      begin
        LoadProperties(LDataSet);
        Result := True;
      end;
    finally
      LDataSet.Free;
    end;
  finally
    LConnection.Free;
  end;
end;

function TModelChat.DoSave: Boolean;
const
  UPDATE_CHAT = 'UPDATE CHAT SET LAST = :LAST, ' +
                                'STOREDDATA = :STOREDDATA, ' +
                                'QUESTION_ID = :QUESTION_ID ' +
                'WHERE ID = :ID';


var
  LConnection: TConnection;
begin
  LConnection := TConnection.GetConnection;
  try
    Result := LConnection.FDConn.ExecSQL(UPDATE_CHAT,
                                         [Now, Self.STOREDDATA, Self.QUESTION_ID, Self.ID],
                                         [ftDateTime, ftString, ftInteger, ftInteger]) > 0;
  finally
    LConnection.Free;
  end;
end;

class function TModelChat.FindOrCreate(AContactID: Integer; out ACreated: Boolean): TModelChat;
var
  LChat: TModelChat;
begin
  ACreated := False;

  LChat := TModelChat.Create;

  LChat.CONTACT_ID := AContactID;

  if not LChat.DoFind then
  begin
    LChat.DoCreate;
    LChat.DoFind;
    ACreated := True;
  end;

  Result := LChat;
end;

function TModelChat.GetData(AData: String): String;
var
  LData: TStrings;
begin
  LData := TStringList.Create;
  try
    LData.Delimiter := '&';
    LData.DelimitedText := Self.STOREDDATA;

    Result := LData.Values[AData];
  finally
    LData.Free;
  end;
end;

procedure TModelChat.LoadProperties(ADataSet: TDataSet);
begin
  ID := ADataSet.FieldByName('ID').AsInteger;
  START := ADataSet.FieldByName('START').AsDateTime;
  LAST := ADataSet.FieldByName('LAST').AsDateTime;
  FINISH := ADataSet.FieldByName('FINISH').AsDateTime;
  STOREDDATA := ADataSet.FieldByName('STOREDDATA').AsString;
  ACTION_TYPE := ADataSet.FieldByName('ACTION_TYPE').AsString;
  QUESTION_ID := ADataSet.FieldByName('QUESTION_ID').AsInteger;
  CONTACT_ID := ADataSet.FieldByName('CONTACT_ID').AsInteger;
end;

function TModelChat.Save: Boolean;
const
  UPDATE = 'UPDATE CHAT SET FINISH = :FINISH WHERE ID = :ID';

var
  LConnection: TConnection;
begin
  LConnection := TConnection.GetConnection;
  try
    Result := LConnection.FDConn.ExecSQL(UPDATE, [Self.FINISH, Self.ID], [ftDateTime, ftInteger]) > 0;
  finally
    LConnection.Free;
  end;
end;

procedure TModelChat.SetACTION_TYPE(const Value: String);
begin
  FACTION_TYPE := Value;
end;

procedure TModelChat.SetCONTACT_ID(const Value: Integer);
begin
  FCONTACT_ID := Value;
end;

procedure TModelChat.SetFINISH(const Value: TDateTime);
begin
  FFINISH := Value;
end;

procedure TModelChat.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TModelChat.SetLAST(const Value: TDateTime);
begin
  FLAST := Value;
end;

procedure TModelChat.SetQuestion(AQuestionID: Integer; ADataToStore: String; AClearStoredData: Boolean);
begin
  if not ADataToStore.Trim.IsEmpty then
  begin
    Self.STOREDDATA := IfThen(Self.STOREDDATA.Trim.IsEmpty, Self.STOREDDATA + '&' + ADataToStore);
  end;

  Self.QUESTION_ID := AQuestionID;

  DoSave;
end;

procedure TModelChat.SetQUESTION_ID(const Value: Integer);
begin
  FQUESTION_ID := Value;
end;

procedure TModelChat.SetSTART(const Value: TDateTime);
begin
  FSTART := Value;
end;

procedure TModelChat.SetSTOREDDATA(const Value: String);
begin
  FSTOREDDATA := Value;
end;

end.
