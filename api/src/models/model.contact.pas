unit model.contact;

interface

uses
  System.SysUtils, Data.DB;

type
  TModelContact = class
  private
    FNAME: String;
    FID: Integer;
    FPHONE_NUMBER: String;
    procedure SetID(const Value: Integer);
    procedure SetNAME(const Value: String);
    procedure SetPHONE_NUMBER(const Value: String);

    function DoFind: Boolean;
    function DoCreate: Boolean;
    procedure LoadProperties(ADataSet: TDataSet);
  public
    property ID: Integer read FID write SetID;
    property NAME: String read FNAME write SetNAME;
    property PHONE_NUMBER: String read FPHONE_NUMBER write SetPHONE_NUMBER;

    class function FindOrCreate(APhoneNumber: String; out ACreated: Boolean): TModelContact;

    function Save: Boolean;
  end;

implementation

{ TModelContact }

uses module.connection;

function TModelContact.DoCreate: Boolean;
const
  INSERT = 'INSERT INTO CONTACT (NAME, PHONE_NUMBER) VALUES (:NAME, :PHONE_NUMBER)';

var
  LConnection: TConnection;
begin
  LConnection := TConnection.GetConnection;
  try
    Result := LConnection.FDConn.ExecSQL(INSERT, [Self.NAME, Self.PHONE_NUMBER], [ftString, ftString]) > 0;
  finally
    LConnection.Free;
  end;
end;

function TModelContact.DoFind: Boolean;
const
  FIND_BY_PHONE = 'SELECT * FROM CONTACT WHERE PHONE_NUMBER = ''%s''';

var
  LConnection: TConnection;
  LDataSet: TDataSet;
begin
  Result := False;

  LConnection := TConnection.GetConnection;
  try
    LConnection.FDConn.ExecSQL(Format(FIND_BY_PHONE, [Self.PHONE_NUMBER]), LDataSet);
    try
      if not LDataSet.IsEmpty then
      begin
        LoadProperties(LDataSet);
        Result := True;
      end;
    finally
      LDataSet.Free;
    end;
  finally
    LConnection.Free;
  end;
end;

class function TModelContact.FindOrCreate(APhoneNumber: String; out ACreated: Boolean): TModelContact;
var
  LPhoneNumber: String;
  LContact: TModelContact;
begin
  ACreated := False;

  LPhoneNumber := Copy(APhoneNumber, Pos(':', APhoneNumber) + 1);

  LContact := TModelContact.Create;

  LContact.PHONE_NUMBER := LPhoneNumber;

  if not LContact.DoFind then
  begin
    LContact.DoCreate;
    LContact.DoFind;
    ACreated := True;
  end;

  Result := LContact;
end;

procedure TModelContact.LoadProperties(ADataSet: TDataSet);
begin
  Self.ID := ADataSet.FieldByName('ID').AsInteger;
  Self.NAME := ADataSet.FieldByName('NAME').AsString;
  Self.PHONE_NUMBER := ADataSet.FieldByName('PHONE_NUMBER').AsString;
end;

function TModelContact.Save: Boolean;
const
  UPDATE = 'UPDATE CONTACT SET NAME = :NAME WHERE ID = :ID';

var
  LConnection: TConnection;
begin
  LConnection := TConnection.GetConnection;
  try
    Result := LConnection.FDConn.ExecSQL(UPDATE, [Self.NAME, Self.ID], [ftString, ftInteger]) > 0;
  finally
    LConnection.Free;
  end;
end;

procedure TModelContact.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TModelContact.SetNAME(const Value: String);
begin
  FNAME := Value;
end;

procedure TModelContact.SetPHONE_NUMBER(const Value: String);
begin
  FPHONE_NUMBER := Value;
end;

end.
