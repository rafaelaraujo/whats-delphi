﻿unit controller.message;

interface

uses
  System.SysUtils, System.Classes, System.StrUtils, Web.HTTPApp, System.NetEncoding;

type
  TControllerMessage = class
  public
    procedure Callback(ARequest: TWebRequest; AResponse: TWebResponse);
    procedure Receive(ARequest: TWebRequest; AResponse: TWebResponse);
    procedure Send(ARequest: TWebRequest; AResponse: TWebResponse);
  end;

const
  ACCOUNT_SID = 'YOUR_ACCOUNT_SID';
  AUTH_TOKEN  = 'YOUR_AUTH_TOKEN';

  SUCCESS_STATUS_CODE = 200;
  SUCCESS_MESSAGE = 'OK';

  PERGUNTA_NAO_TE_CONHECO = 1;
  PERGUNTA_INICIO_CONVERSA = 2;
  PERGUNTA_AGENDAMENTO_CONSULTA = 5;

implementation

{ TControllerMessage }

uses provider.messenger, provider.message, provider.read, model.contact, model.question, model.chat, model.answer;

procedure TControllerMessage.Callback(ARequest: TWebRequest; AResponse: TWebResponse);
var
  LContent: String;
begin
  LContent := ARequest.Content;
end;

procedure TControllerMessage.Receive(ARequest: TWebRequest; AResponse: TWebResponse);
var
  LMessage: TMessage;
  LContact: TModelContact;
  LContactCreated: Boolean;
  LChat: TModelChat;
  LChatCreated: Boolean;
  LQuestion: TModelQuestion;
  LNextQuestion: TModelQuestion;
  LAnswer: TModelAnswer;

  LIdQuestion: Integer;
  LResponse: String;
begin
  AResponse.StatusCode := SUCCESS_STATUS_CODE;
  AResponse.Content := SUCCESS_MESSAGE;

  LMessage := TRead.StringToMessage(ARequest.Content);
  try
    {$REGION 'Keep Alive'}
    if LMessage.Body.Trim.ToUpper = 'PING' then
    begin
      TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, LMessage.&To, LMessage.From, 'pong');
      Exit;
    end;
    {$ENDREGION}

    LContact := TModelContact.FindOrCreate(LMessage.From, LContactCreated);
    try
      LChat := TModelChat.FindOrCreate(LContact.ID, LChatCreated);
      try
        {$REGION 'Novo Contato'}
        if LContactCreated then
        begin
          LQuestion := TModelQuestion.Find(PERGUNTA_NAO_TE_CONHECO);
          try
            TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, LMessage.&To, LMessage.From, LQuestion.QUESTION, LQuestion.MEDIAURL);

            LChat.SetQuestion(PERGUNTA_NAO_TE_CONHECO);

            Exit;
          finally
            LQuestion.Free;
          end;
        end;
        {$ENDREGION}



        {$REGION 'Gravar nome do contato'}
        if LChat.QUESTION_ID = PERGUNTA_NAO_TE_CONHECO then
        begin
          if LMessage.Body.Trim.Length <= 3 then
          begin
            TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, LMessage.&To, LMessage.From,
              'Este não parece ser um nome válido. Tente novamente');
            Exit;
          end;

          LContact.NAME := LMessage.Body;
          LContact.Save;

          LQuestion := TModelQuestion.Find(PERGUNTA_INICIO_CONVERSA);
          try
            TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, LMessage.&To, LMessage.From,
              Format(LQuestion.QUESTION, [LContact.NAME]), LQuestion.MEDIAURL);

            LChat.SetQuestion(LQuestion.ID);
          finally
            LQuestion.Free;
          end;

          Exit;
        end;
        {$ENDREGION}


        if LChat.QUESTION_ID = PERGUNTA_AGENDAMENTO_CONSULTA then
        begin
          if LMessage.Body.Trim = 'F' then
          begin
            LChat.FINISH := Now;
            LChat.Save;

            TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, LMessage.&To, LMessage.From,
                'Até a próxima 😉');

            Exit;
          end;


          TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, LMessage.&To, LMessage.From,
              Format('Consulta com a _especialidade_ *%s* agendada com sucesso', [LChat.GetData('Especialidade')]));

          Exit;
        end;


        {$REGION 'Nova Conversa'}
        if LChatCreated then
        begin
          LQuestion := TModelQuestion.Find(PERGUNTA_INICIO_CONVERSA);
          try
            TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, LMessage.&To, LMessage.From,
             Format(LQuestion.QUESTION, [LContact.NAME]), LQuestion.MEDIAURL);

            LChat.SetQuestion(PERGUNTA_INICIO_CONVERSA);

            Exit;
          finally
            LQuestion.Free;
          end;
        end;
        {$ENDREGION}

        {$REGION 'Encontrar Resposta para a pergunta'}
        LAnswer := TModelAnswer.Find(LChat.QUESTION_ID, LMessage.Body);
        try
          if LAnswer.ID = 0 then
          begin
            TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, LMessage.&To, LMessage.From, 'Não Entendi!');
            Exit;
          end;

          LQuestion := TModelQuestion.Find(LAnswer.NEXTQUESTION_ID);
          try
            {Perguntas aninhadas}
            if LQuestion.NEXT_ID > 0 then
            begin
              LResponse := LQuestion.QUESTION;
              LResponse := LResponse +
                           sLineBreak + sLineBreak + '----------------------' + sLineBreak + sLineBreak;

              LNextQuestion := TModelQuestion.Find(LQuestion.NEXT_ID);
              try
                LResponse := LResponse + LNextQuestion.QUESTION;
                LIdQuestion := LNextQuestion.ID;
              finally
                LNextQuestion.Free;
              end;
            end
            else
            begin
              LIdQuestion := LQuestion.ID;
              LResponse := LQuestion.QUESTION;
            end;

            TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN, LMessage.&To, LMessage.From, LResponse, LQuestion.MEDIAURL);

            LChat.SetQuestion(LIdQuestion, LQuestion.STOREDATA, LQuestion.CLEARDATA = 'S');
          finally
            LQuestion.Free;
          end;
        finally
          LAnswer.Free;
        end;
        {$ENDREGION}
      finally
        LChat.Free;
      end;
    finally
      LContact.Free;
    end;
  finally
    LMessage.Free;
  end;
end;

procedure TControllerMessage.Send(ARequest: TWebRequest; AResponse: TWebResponse);
var
  LQuestion: TModelQuestion;
begin
  LQuestion := TModelQuestion.Find(ARequest.Content.tointeger);
  try
    if LQuestion.ID = 0 then
    begin
      TMessenger.Send(ACCOUNT_SID, AUTH_TOKEN,
                      'whatsapp:+14155238886',
                      'whatsapp:+5511941368119',
                      'Não achei a pergunta solicitada');
      Exit;
    end;


    TMessenger.Send(ACCOUNT_SID,
                    AUTH_TOKEN,
                    'whatsapp:+14155238886',
                    'whatsapp:+5511941368119',
                    LQuestion.QUESTION);
  finally
    LQuestion.Free;
  end;
end;

end.
