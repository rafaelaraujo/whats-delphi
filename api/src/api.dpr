program api;
{$APPTYPE GUI}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  view.main in 'views\view.main.pas' {Form1},
  module.webmodule in 'modules\module.webmodule.pas' {WebModule1: TWebModule},
  controller.message in 'controllers\controller.message.pas',
  provider.message in 'providers\provider.message.pas',
  provider.messenger in 'providers\provider.messenger.pas',
  service.twilio in 'services\service.twilio.pas',
  provider.read in 'providers\provider.read.pas',
  model.contact in 'models\model.contact.pas',
  module.connection in 'modules\module.connection.pas' {Connection: TDataModule},
  model.question in 'models\model.question.pas',
  model.chat in 'models\model.chat.pas',
  model.answer in 'models\model.answer.pas';

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
