unit module.webmodule;

interface

uses
  System.SysUtils, System.Classes, Web.HTTPApp, System.NetEncoding, Vcl.Dialogs;

type
  TWebModule1 = class(TWebModule)
    procedure WebModule1DefaultHandlerAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1actMessageReceiveAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModule1actMessageCallbackAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
    procedure WebModule1actMessageReceberAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
    procedure WebModule1actMessageSendAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WebModuleClass: TComponentClass = TWebModule1;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses controller.message;

{$R *.dfm}

procedure TWebModule1.WebModule1actMessageCallbackAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
  var Handled: Boolean);
var
  LControllerMessage: TControllerMessage;
begin
  LControllerMessage := TControllerMessage.Create;
  try
    LControllerMessage.Callback(Request, Response);
  finally
    LControllerMessage.Free;
  end;
end;





procedure TWebModule1.WebModule1actMessageReceberAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
  var Handled: Boolean);
var
  LContent: String;
begin
  LContent := Request.Content;
  ShowMessage(LContent);
end;






procedure TWebModule1.WebModule1actMessageReceiveAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
  LControllerMessage: TControllerMessage;
begin
  LControllerMessage := TControllerMessage.Create;
  try
    LControllerMessage.Receive(Request, Response);
  finally
    LControllerMessage.Free;
  end;
end;

procedure TWebModule1.WebModule1actMessageSendAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
  var Handled: Boolean);
  var
  LControllerMessage: TControllerMessage;
begin
  LControllerMessage := TControllerMessage.Create;
  try
    LControllerMessage.Send(Request, Response);
  finally
    LControllerMessage.Free;
  end;
end;

procedure TWebModule1.WebModule1DefaultHandlerAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  Response.Content := '<html>' +
    '<head><title>Web Server Application</title></head>' +
    '<body>Web Server Application</body>' + '</html>';
end;

end.
