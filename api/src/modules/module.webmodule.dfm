object WebModule1: TWebModule1
  OldCreateOrder = False
  Actions = <
    item
      Default = True
      Name = 'DefaultHandler'
      PathInfo = '/'
      OnAction = WebModule1DefaultHandlerAction
    end
    item
      MethodType = mtPost
      Name = 'actMessageReceive'
      PathInfo = '/api/messages/receive'
      OnAction = WebModule1actMessageReceiveAction
    end
    item
      MethodType = mtPost
      Name = 'actMessageCallback'
      PathInfo = '/api/messages/callback'
      OnAction = WebModule1actMessageCallbackAction
    end
    item
      MethodType = mtPost
      Name = 'actMessageReceber'
      PathInfo = '/api/messages/receber'
      OnAction = WebModule1actMessageReceberAction
    end
    item
      MethodType = mtPost
      Name = 'actMessageSend'
      PathInfo = '/api/messages/send'
      OnAction = WebModule1actMessageSendAction
    end>
  Height = 300
  Width = 511
end
