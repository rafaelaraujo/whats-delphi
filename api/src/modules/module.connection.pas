unit module.connection;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.IB,
  FireDAC.Phys.IBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, FireDAC.DApt;

type
  TConnection = class(TDataModule)
    FDConn: TFDConnection;
  private
    { Private declarations }
  public
    { Public declarations }
    class function GetConnection: TConnection;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TConnection }

class function TConnection.GetConnection: TConnection;
begin
  Result := TConnection.Create(nil);
end;

end.
