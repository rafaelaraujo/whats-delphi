unit view.main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.pngimage, Vcl.ExtCtrls,
  Vcl.StdCtrls;

type
  TfrmViewMain = class(TForm)
    memMessage: TMemo;
    btnSend: TButton;
    Label1: TLabel;
    Label2: TLabel;
    edtMediaURL: TEdit;
    Image1: TImage;
    Label3: TLabel;
    Label4: TLabel;
    edtNumerTo: TEdit;
    procedure btnSendClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmViewMain: TfrmViewMain;

implementation

{$R *.dfm}

uses controller.main;

procedure TfrmViewMain.btnSendClick(Sender: TObject);
begin
  if TControllerMain.SendMessage(edtNumerTo.Text, memMessage.Text, edtMediaURL.Text) then
  begin
    ShowMessage('Mensagem enviada com sucesso!');
  end;
end;

end.
