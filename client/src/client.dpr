program client;

uses
  Vcl.Forms,
  view.main in 'views\view.main.pas' {frmViewMain},
  service.twilio in 'services\service.twilio.pas',
  controller.main in 'controllers\controller.main.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmViewMain, frmViewMain);
  Application.Run;
end.
